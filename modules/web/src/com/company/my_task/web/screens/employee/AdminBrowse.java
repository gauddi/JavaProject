package com.company.my_task.web.screens.employee;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.View;
import com.haulmont.cuba.core.global.ViewBuilder;
import com.haulmont.cuba.gui.screen.*;
import com.company.my_task.entity.Employee;
import com.haulmont.cuba.security.entity.User;

import javax.inject.Inject;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;


@UiController("mytask_Employee.browse.adm")
@UiDescriptor("employee-browse-adm.xml")
@LookupComponent("employeesTable")
@LoadDataBeforeShow
public class AdminBrowse extends StandardLookup<Employee> {
    public void getFromTable() {
        try{
            URL url = new URL("https://script.google.com/macros/s/AKfycbyIkO5_wvTZG5RfyPmbpIaGR4rqlnqh_QDIYcdzi8s21UUZ8bYjTAXoc3_uJnNmiqRMBg/exec");
            URLConnection urlConnection = url.openConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    @Inject
    private DataManager dataManager;
//    View view = ViewBuilder.of(User.class)
//            .addAll("login", "password","lasrName")
//            .build();

    View view = ViewBuilder.of(Employee.class)
            .addAll("lastName", "password","emailAdress")
            .build();

    

    private Employee loadEmployee() {
        LoadContext<Employee> loadContext = LoadContext.create(Employee.class).setView(view);
        return dataManager.load(loadContext);

    }


}