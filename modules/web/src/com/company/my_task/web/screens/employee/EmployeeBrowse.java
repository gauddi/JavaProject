package com.company.my_task.web.screens.employee;

import com.haulmont.cuba.gui.screen.*;
import com.company.my_task.entity.Employee;
import com.haulmont.cuba.security.global.UserSession;

@UiController("mytask_Employee.browse")
@UiDescriptor("employee-browse.xml")
@LookupComponent("employeesTable")
@LoadDataBeforeShow
public class EmployeeBrowse extends StandardLookup<Employee> {
    public void onBeforeShow(BeforeShowEvent event) {
        UserSession UserSession = new UserSession();
        String login = UserSession.getUser().getLogin();
    }
}