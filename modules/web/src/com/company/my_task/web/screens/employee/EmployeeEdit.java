package com.company.my_task.web.screens.employee;

import com.haulmont.cuba.gui.screen.*;
import com.company.my_task.entity.Employee;

@UiController("mytask_Employee.edit")
@UiDescriptor("employee-edit.xml")
@EditedEntityContainer("employeeDc")
@LoadDataBeforeShow
public class EmployeeEdit extends StandardEditor<Employee> {
}