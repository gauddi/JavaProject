create table MYTASK_EMPLOYEE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    LAST_NAME varchar(255) not null,
    FIRST_NAME varchar(255) not null,
    MIDDLE_NAME varchar(255) not null,
    EMAIL_ADRESS varchar(255) not null,
    PHONE_NUMBER varchar(255) not null,
    POST varchar(255) not null,
    COMPANY varchar(255) not null,
    PASSWORD varchar(255) not null,
    CHECKBOX varchar(255) not null,
    --
    primary key (ID)
);